<?php
include('conexion.php');

//listar medicamento

function listarmedicamento(){
    $pdo = Conexion::conectar();
    $consulta = $pdo->prepare("SELECT Codigo,
    NombreMedicamento,
    Descripcion,
    Precio,
    NombreVendedor  FROM medicamento");
    $consulta->execute();
    $resultado=$consulta->fetchAll();
    $list = array();



    //llenando de array el cual se obtendra como resultado en el servicio
    foreach($resultado as $row){
        array_push($list, array(
      'Codigo'=>$row['Codigo'],
      'NombreMedicamento'=>$row['NombreMedicamento'],
      'Descripcion'=>$row['Descripcion'],
      'Precio'=>$row['Precio'],
      'NombreVendedor'=>$row['NombreVendedor']
        ));
    }
    return array("result"=>true,"medicamento" => $list);
}

//tabla 2

//listar empleado

function listarempleado(){
    $pdo = Conexion::conectar();
    $consulta = $pdo->prepare("SELECT 	
    CodigoEmpleado,
    NombreEmpleado,
    ApellidoEmpleado,
    Direccion,
    sueldo FROM empleado");
    $consulta->execute();
    $resultado=$consulta->fetchAll();
    $list = array();


    //llenando de array el cual se obtendra como resultado en el servicio 
    foreach($resultado as $row){
        array_push($list, array(
      'CodigoEmpleado'=>$row['CodigoEmpleado'],
      'NombreEmpleado'=>$row['NombreEmpleado'],
      'ApellidoEmpleado'=>$row['ApellidoEmpleado'],
      'Direccion'=>$row['Direccion'],
      'sueldo'=>$row['sueldo']
        ));
    }
    return array("result"=>true,"empleado" => $list);
}

//registro de alumno 

function nuevomedicamento($Codigo,$NombreMedicamento,$Descripcion,$Precio
,$NombreVendedor){
    $pdo = Conexion::conectar();
    if($pdo){
        $consulta = " INSERT INTO medicamento VALUES('".$Codigo."','".$NombreMedicamento."','".$Descripcion."','".$Precio."','".$NombreVendedor."')";
        $pdo->prepare($consulta)
        ->execute();
        return true;

    }
    return false;
}
//tabla 2

//registro de empleado

function nuevoempleado($CodigoEmpleado,$NombreEmpleado,$ApellidoEmpleado,$Direccion
,$sueldo){
    $pdo = Conexion::conectar();
    if($pdo){
        $consulta = " INSERT INTO empleado VALUES('".$CodigoEmpleado."','".$NombreEmpleado."',
        '".$ApellidoEmpleado."','".$Direccion."','".$sueldo."')";
        $pdo->prepare($consulta)
        ->execute();
        return true;

    }
    return false;
}


//busquedad registro por ID

function buscarmedicamento($Codigo){
$pdo = Conexion::conectar();
$consulta = $pdo->prepare(" SELECT Codigo,
NombreMedicamento,
Descripcion,
Precio,
NombreVendedor FROM medicamento WHERE Codigo='".$Codigo."'");
$consulta->execute();
$resultado=$consulta->fetchAll();
$list = array();

foreach($resultado as $row){
    array_push($list, array(
        'Codigo'=>$row['Codigo'],
        'NombreMedicamento'=>$row['NombreMedicamento'],
        'Descripcion'=>$row['Descripcion'],
        'Precio'=>$row['Precio'],
        'NombreVendedor'=>$row['NombreVendedor']
    ));
}
return array("result"=> true,"medicamento" => $list);
}

//tabla 2

//busquedad registro por ID

function buscarempleado($CodigoEmpleado){
    $pdo = Conexion::conectar();
    $consulta = $pdo->prepare(" SELECT CodigoEmpleado,
    NombreEmpleado,
        ApellidoEmpleado,
        Direccion,
        sueldo FROM empleado WHERE CodigoEmpleado='".$CodigoEmpleado."'");
    $consulta->execute();
    $resultado=$consulta->fetchAll();
    $list = array();
    
    foreach($resultado as $row){
        array_push($list, array(
            'CodigoEmpleado'=>$row['CodigoEmpleado'],
            'NombreEmpleado'=>$row['NombreEmpleado'],
            'ApellidoEmpleado'=>$row['ApellidoEmpleado'],
            'Direccion'=>$row['Direccion'],
            'sueldo'=>$row['sueldo']
        ));
    }
    return array("result"=> true,"empleado" => $list);
    }

function actualizarmedicamento($Codigo,$NombreMedicamento,$Descripcion,$Precio,$NombreVendedor){
    $pdo = Conexion::conectar();
    if($pdo){
        $consulta = "UPDATE medicamento SET NombreMedicamento='".$NombreMedicamento."',Descripcion='".$Descripcion."',Precio='".$Precio."',NombreVendedor='".$NombreVendedor."'
         WHERE Codigo='".$Codigo."'";
         $pdo->prepare($consulta)->execute();
         return true;

    }
    return false;
}

//tabla2
function actualizarempleado($CodigoEmpleado,$NombreEmpleado,$ApellidoEmpleado,$Direccion
,$sueldo){
    $pdo = Conexion::conectar();
    if($pdo){
        $consulta = "UPDATE empleado SET NombreEmpleado='".$NombreEmpleado."',ApellidoEmpleado='".$ApellidoEmpleado."',
        Direccion='".$Direccion."',sueldo='".$sueldo."'
         WHERE CodigoEmpleado='".$CodigoEmpleado."'";
         $pdo->prepare($consulta)->execute();
         return true;

    }
    return false;
}

//eliminar registros

function eliminarmedicamentos($Codigo){
    $pdo = Conexion::conectar();
    if($pdo){
        $consulta = "DELETE FROM medicamento  WHERE Codigo='".$Codigo."'";
    $pdo->prepare($consulta)->execute();
    return true;

}
return false;

}

//tabla 2
//eliminar registros

function eliminarempleado($CodigoEmpleado){
    $pdo = Conexion::conectar();
    if($pdo){
        $consulta = "DELETE FROM empleado   WHERE CodigoEmpleado='".$CodigoEmpleado."'";
    $pdo->prepare($consulta)->execute();
    return true;

}
return false;

}


